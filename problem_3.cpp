//The prime factors of 13195 are 5, 7, 13 and 29.
//What is the largest prime factor of the number 600851475143

#include <iostream>
#include <math.h>

using namespace std;

bool isPrime(int num){
	const int limit = (int) ceil(sqrt(num));

	for(int i=2; i<=limit; i++){
		if(num % limit == 0){
			return false;
		}
	}
	return true;
}

int main(){
	
	const long NUM = 600851475143;
	int limit = (int) ceil(sqrt(NUM));

	for(int i = limit; i>1; i-=2){
		
		if(NUM%i == 0){
			if(isPrime(i)){
				cout<<"result: "<< i << endl;
			}	
		}
	}

	return 0;
}
