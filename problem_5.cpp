//2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
//What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
//solved

#include <iostream>
#include <vector>

using namespace std;


int* generateUniqueDivisors(int range){

	int *pDivisors = new int[range+1];

	for(int i=0; i<range+1; i++){
		pDivisors[i] = i;
	}

	for(int i=2; i<range+1; i++){
		for(int j=2; j<i; j++){
			if((i%j) == 0){
				pDivisors[j] = 1;
			}
		}
	}

	return pDivisors;
}


int findSmallest(int range){
	int *pDivisors = generateUniqueDivisors(range);
	int number = range;
	bool flag = true;

	while(number>0){
		for(int i=2; i<range+1; i++){
			if(pDivisors[i] >1){
				if((number%pDivisors[i]) != 0){
					flag = false;	
					break;
				}
			}
		}
		
		if(flag){
			return number;
		}
		flag = true;
		number++;
	}
	return -1;
}


int main(){
	
	int range = 20;
	cout << findSmallest(range) << endl;
	return 0;
}
