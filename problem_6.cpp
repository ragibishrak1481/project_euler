//solved

#include <iostream>
#include <vector>

using namespace std;


int main(){

	int sum;
	int square_sum;
	
	sum = 0;
	square_sum = 0;

	for(int i=1; i<=100; i++){
		sum += i;
		square_sum += i*i;
	}
	cout << square_sum - (sum*sum) << endl;
	return 0;
}
