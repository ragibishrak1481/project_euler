//By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
//What is the 10001st prime number?
//solved
//
//
#include <iostream>
#include <vector>

using namespace std;


int getPrime(int nPrime){

	vector<int> primes;
	int primeCount = 0;
	bool flag = true;
	
	primes.push_back(2);
	primeCount++;

	for(int num=3; primeCount < nPrime; num++){

		for(auto prime: primes){
			if((num%prime) == 0){
				flag = false;
				break;
			}
		}

		if(flag){
			primeCount++;
			primes.push_back(num);
		}
		flag = true;
	}

	return primes.at(nPrime-1);	
}


int main(){	
	cout << getPrime(10001) << endl;
	return 0;
}
