#include <iostream>
#include <vector>

using namespace std;

// Function to check if a number is palindrome
bool isPalindrome(int number){

	vector<int> number_break;
	bool palindrome = true;	

	while(number>0){
		number_break.push_back(number%10);
		number = number/10;
	}

	int index_front = 0;
	int index_back = number_break.size()-1;

	while(index_front <= index_back){
		
        if(number_break.at(index_front) != number_break.at(index_back)){
        	palindrome = false;
        }
		index_front++;
		index_back--;
	}
	return palindrome;
}


// check if a palindrome number stisfy the condition in the ques
bool doesSatisfyCondition(int number){
	
	int quotient;
	for(int i=999; i>99; i--){
		if((number % i) == 0){
			quotient = number/i;
			if((quotient > 99) && (quotient <1000)){
				return true;
			}
		}
	}
	return false;
}


int main(){
	
	for(int i=999*999; i>100; i--){
		
		if(isPalindrome(i)){
			if(doesSatisfyCondition(i)){
				cout << "The number is: " << i << endl;
				break;
			}
		}
	}
	return 0;
}
