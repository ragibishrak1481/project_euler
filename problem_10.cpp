#include <iostream>
#include <vector>

using namespace std;


long long unsigned int getPrimeSum(int limit){

	vector<int> primes;
	bool flag = true;
	int pres_max = 2;	

	primes.push_back(2);

	for(int num=3; pres_max < limit; num++){

		for(auto prime: primes){
			if((num%prime) == 0){
				flag = false;
				break;
			}
		}

		if(flag){	
			pres_max = num;
			primes.push_back(num);
		}
		flag = true;
		if(num > limit)
			break;
	}

	long long unsigned int sum = 0;

	for(auto prime: primes)
		sum += prime;

	return sum;
}


int main(){

	cout << getPrimeSum(2e6) << endl;
	return 0;
}
