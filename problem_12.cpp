#include <iostream>
#include <vector>

using namespace std;


int numFactors(long long unsigned int num){
	int count = 0;
	for(long long unsigned int i=1; i<=num; i++){
		if((num%i) == 0){
			count++;
		}
	}

	return count;
}


int main(){
	
	long long unsigned int sum = 0;

	for(int num=1; ; num++){
		sum += num;
		if(numFactors(sum) > 500){
			cout << sum << endl;
			break;
		}
	}

	return 0;
}
